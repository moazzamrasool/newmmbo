<!DOCTYPE html>
<html>
  <head>
   @include('layouts.head')
  </head>
  <body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
      @include('layouts.header')
      </header>
      <!-- Left side column. contains the logo and sidebar -->
     @include('layouts.sidenav')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         @section('bredcrumb')
         @show
        </section>

        {{-- <!-- Main content -->
        <section class="content">
          <!-- info cards -->
        @section('info-cards')
         @show
   
        </section><!-- /.content --> --}}
        @section('main-content')
        @show
      </div><!-- /.content-wrapper -->
      @include('layouts.footer')
  </body>
</html>
