<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class SubAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
public function handle($request, Closure $next)
    {
    if(auth()->check()){
    if (auth()->user()->isAdmin == 1) {
            return redirect('admin-dashboard');
            }
            elseif(auth()->user()->isAdmin == 2 ){
            if(auth()->user()->status == 0){
                Auth::logout();
            return redirect('/login');
            }
            else{
                
                return $next($request); 
            }
            
            }

            
            elseif(auth()->user()->isAdmin == 3){
                return redirect('home');

            }
    else{
        return redirect('/login');

    }

    }
    else{
        return redirect('/login');
    }
    }

}